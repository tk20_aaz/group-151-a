library(tidyverse)
df<- read.csv("30_Auto_theft.csv") 
ans <- df %>% replace(.=="NULL", 0)

my_data_mcj <- subset(ans, Group_Name == "AT2-Motor Car/Taxi/Jeep")
my_data_mcs <- subset(ans, Group_Name == "AT1-Motor Cycles/ Scooters")


a <- c(my_data_mcj$Auto_Theft_Recovered)
b <- c(my_data_mcs$Auto_Theft_Recovered)

c <- as.numeric(a)

d <- as.numeric(b)
boxplot(d,c)
hist(d)
hist(c)

t.test(d, c, paired = FALSE)

wilcox <- wilcox.test(x = d, y = c,
            mu=0, alt="two.sided", conf.int=T, conf.level=0.95,
            paired=FALSE, exact=T, correct=T)
print(wilcox)
dev.off()


